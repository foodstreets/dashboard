package route

import (
	"net/http"

	mMiddleware "gitlab.com/foodstreets/master/middleware"

	"gitlab.com/foodstreets/dashboard/api/handler"

	"github.com/gin-gonic/gin"
)

var (
	version = "version"
)

func Init() *gin.Engine {
	r := gin.Default()

	handlerEatery := handler.EateryHandler
	handlerGroupDish := handler.GroupDishHandler
	handlerDish := handler.DishHandler
	handlerSearch := handler.SearchHandler
	handlerAppVersion := handler.AppVersionHandler
	handlerQuickReview := handler.QuickReviewHandler

	r.Use(gin.Recovery())
	r.Use(mMiddleware.CORS())

	//	hashKey := "10116c519f514c8a68f451bbbde6efedb2f3b1b18a368ffcdb4da4aa7e57baf7"
	//	blockKey := "d9b0fddec1df90c5d29bfe56e0f375d0"
	//	securecCookie := mMiddleware.NewSecureCookie(blockKey, hashKey)
	//	authenMiddleware := mMiddleware.NewAuthenMiddleware(securecCookie, middleware.GetCurrentUser)
	//	middleware.InitAuth(authenMiddleware.GetCurrentUser)

	groupVersion := r.Group(":" + version)

	//	groupVersion.Use(authenMiddleware.Interception())
	groupEatery := groupVersion.Group("/eateries")
	{
		POST(groupEatery, "", handlerEatery.Create)
		GET(groupEatery, "/:id", handlerEatery.GetByID)
		//		GET(groupEatery, "/:id", handlerEatery.GetElasticByID)
		DELETE(groupEatery, "/:id", handlerEatery.DeleteByID)
		PUT(groupEatery, "/:id", handlerEatery.Update)

	}

	groupSearch := groupVersion.Group("/search")
	{
		GET(groupSearch, "/eateries", handlerSearch.SearchEatery)
	}

	groupGroupDish := groupEatery.Group("/groups")
	{
		POST(groupGroupDish, "", handlerGroupDish.Create)
		GET(groupGroupDish, "/:id", handlerGroupDish.GetByID)
	}

	groupDish := groupGroupDish.Group("/dishes")
	{
		POST(groupDish, "", handlerDish.Create)
		GET(groupDish, "/:id", handlerDish.GetByID)
	}

	groupAppVersion := groupVersion.Group("/app-versions")
	{
		POST(groupAppVersion, "", handlerAppVersion.Create)
	}

	groupQuickReview := groupVersion.Group("/quick-reviews")
	{
		POST(groupQuickReview, "", handlerQuickReview.Create)
	}

	return r
}

// Override Method
func POST(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodPost, relativePath, handlers)
}
func GET(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodGet, relativePath, handlers)
}
func DELETE(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodDelete, relativePath, handlers)
}
func PUT(group *gin.RouterGroup, relativePath string, handlers func(*gin.Context)) {
	route(group, http.MethodPut, relativePath, handlers)
}

func route(group *gin.RouterGroup, method, relativePath string, handlers func(*gin.Context)) {
	switch method {
	case http.MethodPost:
		group.POST(relativePath, handlers)
	case http.MethodGet:
		group.GET(relativePath, handlers)
	case http.MethodDelete:
		group.DELETE(relativePath, handlers)
	case http.MethodPut:
		group.PUT(relativePath, handlers)
	}
}
