package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	authClient "gitlab.com/foodstreets/auth/init/client"
	"gitlab.com/foodstreets/dashboard/config"
	"gitlab.com/foodstreets/dashboard/route"
	"gitlab.com/foodstreets/master/cmd"
	"gitlab.com/foodstreets/master/infra"
	newsfeedClient "gitlab.com/foodstreets/newsfeed/init/client"
)

func main() {
	cmd.Execute()
	conf := config.Get()

	//Setup Infra
	setupInfra(conf)

	//Init router
	initRoute(conf)
}

func initRoute(conf config.Config) {
	r := route.Init()
	address := fmt.Sprintf("%s:%d", conf.App.Host, conf.App.Port)
	srv := &http.Server{
		Addr:    address,
		Handler: r,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}

func setupInfra(conf config.Config) {

	//Init Psql
	infra.InitMaster(conf.Postgresql)

	//Init Redis
	infra.InitRedis(conf.Redis)

	//AuthGRPC
	authConfig := authClient.GrpcConfig{
		Address: conf.GrpcAuth.Url,
	}
	authClient.InitClient(authConfig)

	//Newsfeed GRPC
	newsfeedConfig := newsfeedClient.GrpcConfig{
		Address: conf.GrpcNewsfeed.Url,
	}
	newsfeedClient.InitClient(newsfeedConfig)
}
