package handler

import (
	"fmt"
	"strconv"

	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/dashboard/api/view"

	"gitlab.com/foodstreets/dashboard/api/entity"

	"github.com/gin-gonic/gin"
)

type eateryHandler struct{}

var EateryHandler IEateryHandler

type IEateryHandler interface {
	Create(c *gin.Context)
	GetByID(c *gin.Context)
	GetElasticByID(c *gin.Context)
	DeleteByID(c *gin.Context)
	Update(c *gin.Context)
}

func init() {
	EateryHandler = &eateryHandler{}
}

func (e eateryHandler) Create(c *gin.Context) {
	//	currentUser := middleware.Auth.GetCurrentUser(c)
	//	if currentUser == nil {
	//		return
	//	}

	formEatery := form.Eatery{}
	if err := formEatery.Bind(c); err != nil {
		panic(err)
		return
	}

	eatery, err := entity.EntityEatery.Create(formEatery)
	if err != nil {
		fmt.Println("err", err)
		return
	}

	resp := view.PopulateEatery(eatery)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (e eateryHandler) GetByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	eatery, err := entity.EntityEatery.GetByID(id)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateEatery(eatery)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (e eateryHandler) GetElasticByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	eatery, err := entity.EntityEatery.GetElasticByID(id)
	if err != nil {
		panic(err)
		return
	}
	if eatery.Id == 0 {
		c.JSON(200, gin.H{
			"data": nil,
		})
		return
	}

	resp := view.NewElasticEatery(eatery)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (e eateryHandler) DeleteByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	err := entity.EntityEatery.DeleteByID(id)
	if err != nil {
		panic(err)
		return
	}
	c.JSON(200, gin.H{
		"message": "done",
	})
}

func (e eateryHandler) Update(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	formEatery := form.Eatery{}
	if err := formEatery.Bind(c); err != nil {
		panic(err)
		return
	}

	resp, err := entity.EntityEatery.Update(id, formEatery)
	if err != nil {
		panic(err)
		return
	}
	c.JSON(200, gin.H{
		"data": resp,
	})
}
