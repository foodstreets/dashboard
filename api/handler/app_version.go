package handler

import (
	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/dashboard/api/entity"

	"github.com/gin-gonic/gin"
)

type appVersionHandler struct{}

var AppVersionHandler IAppVersionHandler

type IAppVersionHandler interface {
	Create(c *gin.Context)
}

func init() {
	AppVersionHandler = &appVersionHandler{}
}

func (a appVersionHandler) Create(c *gin.Context) {
	formAppVersion := form.AppVersion{}
	if err := formAppVersion.Bind(c); err != nil {
		panic(err)
		return
	}

	_, err := entity.AppVersion.Create(formAppVersion)
	if err != nil {
		panic(err)
		return
	}

	c.JSON(200, gin.H{})
}
