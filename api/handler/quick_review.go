package handler

import (
	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/dashboard/api/entity"

	"github.com/gin-gonic/gin"
)

type quickReviewHandler struct{}

var QuickReviewHandler IQuickReviewHandler

type IQuickReviewHandler interface {
	Create(c *gin.Context)
}

func init() {
	QuickReviewHandler = &quickReviewHandler{}
}

func (a quickReviewHandler) Create(c *gin.Context) {
	formQuickReview := form.QuickReview{}
	if err := formQuickReview.Bind(c); err != nil {
		panic(err)
		return
	}

	_, err := entity.QuickReview.Create(formQuickReview)
	if err != nil {
		panic(err)
		return
	}

	c.JSON(200, gin.H{})
}
