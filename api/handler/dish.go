package handler

import (
	"fmt"
	"strconv"

	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/dashboard/api/view"

	"gitlab.com/foodstreets/dashboard/api/entity"

	"github.com/gin-gonic/gin"
)

type dishHandler struct{}

var DishHandler IDishHandler

type IDishHandler interface {
	Create(c *gin.Context)
	GetByID(c *gin.Context)
}

func init() {
	DishHandler = dishHandler{}
}

func (d dishHandler) Create(c *gin.Context) {
	groupDishId, _ := strconv.Atoi(c.Param("id"))
	formDish := form.Dish{}
	if err := formDish.Bind(c); err != nil {
		panic(err)
		return
	}

	dish, err := entity.EntityDish.Create(formDish, groupDishId)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateDish(dish)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (d dishHandler) GetByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	dish, err := entity.EntityDish.GetByID(id)
	if err != nil {
		fmt.Println("err", err)
		return
	}

	resp := view.PopulateDish(dish)
	c.JSON(200, gin.H{
		"data": resp,
	})
}
