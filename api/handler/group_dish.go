package handler

import (
	"strconv"

	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/dashboard/api/view"

	"gitlab.com/foodstreets/dashboard/api/entity"

	"github.com/gin-gonic/gin"
)

type groupDishHandler struct{}

var GroupDishHandler IGroupDishHandler

type IGroupDishHandler interface {
	Create(c *gin.Context)
	GetByID(c *gin.Context)
}

func init() {
	GroupDishHandler = &groupDishHandler{}
}

func (g groupDishHandler) Create(c *gin.Context) {
	slug := c.Param("slug")
	formGroupDish := form.GroupDish{}
	if err := formGroupDish.Bind(c); err != nil {
		panic(err)
		return
	}

	groupDish, err := entity.EntityGroupDish.Create(formGroupDish, slug)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateGroupDish(groupDish)
	c.JSON(200, gin.H{
		"data": resp,
	})
}

func (g groupDishHandler) GetByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	groupDish, err := entity.EntityGroupDish.GetByID(id)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateGroupDish(groupDish)
	c.JSON(200, gin.H{
		"data": resp,
	})
}
