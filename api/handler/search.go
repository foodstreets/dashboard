package handler

import (
	"gitlab.com/foodstreets/dashboard/api/view"

	"gitlab.com/foodstreets/dashboard/api/entity"

	"github.com/gin-gonic/gin"
)

type searchHandler struct{}

var SearchHandler ISearchHandler

type ISearchHandler interface {
	SearchEatery(c *gin.Context)
}

func init() {
	SearchHandler = searchHandler{}
}

type Response struct {
	Data       interface{} `json:"data"`
	Pagination struct {
		Total int `json:"total"`
	} `json:"pagination"`
}

func (searchHandler) SearchEatery(c *gin.Context) {
	total, eateries, err := entity.EntityEatery.SearchEatery(c)
	if err != nil {
		panic(err)
		return
	}

	resp := view.PopulateEateries(eateries)

	data := Response{}
	data.Data = resp
	data.Pagination.Total = total

	c.JSON(200, data)
	return
}
