package view

import (
	"time"

	"gitlab.com/foodstreets/master/model"
)

type GroupDish struct {
	Id        int        `json:"id"`
	Name      string     `json:"name"`
	EateryId  int        `json:"eateryId"`
	Active    bool       `json:"active"`
	CreatedAt *time.Time `json:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
	CreatedBy int        `json:"createdBy"`
	UpdatedBy int        `json:"updatedBy"`
}

func PopulateGroupDish(groupDish *model.GroupDish) GroupDish {
	return GroupDish{
		Id:        groupDish.Id,
		Name:      groupDish.Name,
		EateryId:  groupDish.EateryId,
		Active:    groupDish.Active,
		CreatedBy: groupDish.CreatedBy,
		UpdatedBy: groupDish.UpdatedBy,
	}
}
