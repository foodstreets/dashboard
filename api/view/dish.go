package view

import (
	"time"

	"gitlab.com/foodstreets/master/model"
)

type Dish struct {
	Id          int        `json:"id"`
	Name        string     `json:"name"`
	Slug        string     `json:"slug"`
	Image       string     `json:"image"`
	GroupDishId int        `json:"groupDishId"`
	Price       float32    `json:"price"`
	Active      bool       `json:"active"`
	CreatedAt   *time.Time `json:"createdAt"`
	UpdatedAt   *time.Time `json:"updatedAt"`
	CreatedBy   int        `json:"createdBy"`
	UpdatedBy   int        `json:"updatedBy"`
}

func PopulateDish(dish *model.Dish) Dish {
	if dish == nil {
		return Dish{}
	}

	return Dish{
		Id:          dish.Id,
		Name:        dish.Name,
		Slug:        dish.Slug,
		GroupDishId: dish.GroupDishId,
		Price:       dish.Price,
		Image:       dish.Image,
		Active:      dish.Active,
		CreatedBy:   dish.CreatedBy,
		UpdatedBy:   dish.UpdatedBy,
	}
}
