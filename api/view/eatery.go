package view

import (
	"gitlab.com/foodstreets/newsfeed/protobuf/response"

	"gitlab.com/foodstreets/master/repo"

	"gitlab.com/foodstreets/master/model"
)

type Eatery struct {
	Id      int    `json:"id"`
	Name    string `json:"name"`
	Slug    string `json:"slug"`
	Active  bool   `json:"active"`
	Image   string `json:"image"`
	Address string `json:"address"`
}

func PopulateEatery(eatery *model.Eatery) Eatery {
	if eatery == nil {
		return Eatery{}
	}

	imageURL := repo.Upload.GetImageFoodURL("http://18.138.177.0/cdn/v1", eatery.Image)
	return Eatery{
		Id:      eatery.Id,
		Name:    eatery.Name,
		Slug:    eatery.Slug,
		Image:   imageURL,
		Active:  eatery.Active,
		Address: eatery.Address,
	}
}

func PopulateEateries(eateries []model.Eatery) []Eatery {
	if len(eateries) == 0 {
		return []Eatery{}
	}

	res := make([]Eatery, len(eateries))
	for i, eatery := range eateries {
		res[i] = PopulateEatery(&eatery)
	}
	return res
}

func NewElasticEatery(eatery *response.Eatery) Eatery {
	return Eatery{
		Id:      int(eatery.Id),
		Name:    eatery.Name,
		Slug:    eatery.Slug,
		Image:   eatery.Image,
		Active:  eatery.Active,
		Address: eatery.Address,
	}
}
