package form

import (
	"gitlab.com/foodstreets/master/model"

	"github.com/gin-gonic/gin"
)

type QuickReview struct {
	TypeName string `json:"typeName"`
	Image    string `json:"image"`
}

func (input *QuickReview) Bind(c *gin.Context) (err error) {
	err = c.Bind(&input)
	return
}

func (input *QuickReview) ToModel() model.QuickReview {
	return model.QuickReview{
		TypeName: input.TypeName,
		Image:    input.Image,
	}
}
