package form

import (
	"time"

	"gitlab.com/foodstreets/master/model"

	"github.com/gin-gonic/gin"
)

type Eatery struct {
	Name       string `json:"name"`
	Address    string `json:"address"`
	Image      string `json:"image"`
	CityId     int    `json:"cityId"`
	DistrictId int    `json:"districtId"`
	//	OpenTime   time.Time `json:"openTime"`
	//	CloseTime  time.Time `json:"closeTime"`
}

func (input *Eatery) Bind(c *gin.Context) (err error) {
	err = c.Bind(&input)
	return
}

func (input Eatery) ToModel(userID int) model.Eatery {
	return model.Eatery{
		Name:       input.Name,
		Address:    input.Address,
		Image:      input.Image,
		CityId:     input.CityId,
		DistrictId: input.DistrictId,
		UpdatedAt:  time.Now(),
		UpdatedBy:  userID,
	}
}
