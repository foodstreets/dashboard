package form

import "github.com/gin-gonic/gin"

type Dish struct {
	Name   string  `json:"name"`
	Slug   string  `json:"slug"`
	Active bool    `json:"active"`
	Image  string  `json:"image"`
	Price  float32 `json:"price"`
}

func (input *Dish) Bind(c *gin.Context) (err error) {
	err = c.Bind(&input)
	return
}
