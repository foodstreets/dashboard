package form

import "github.com/gin-gonic/gin"

type GroupDish struct {
	Name string `json:"name"`
}

func (input *GroupDish) Bind(c *gin.Context) (err error) {
	err = c.Bind(&input)
	return
}
