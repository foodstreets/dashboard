package form

import (
	"gitlab.com/foodstreets/master/model"

	"github.com/gin-gonic/gin"
)

type AppVersion struct {
	Version     string `json:"version"`
	Title       string `json:"title"`
	Message     string `json:"message"`
	Type        string `json:"type"`
	BuildNumber int    `json:"buildNumber"`
}

func (input *AppVersion) Bind(c *gin.Context) (err error) {
	err = c.Bind(&input)
	return
}

func (input *AppVersion) ToModel() model.AppVersion {
	return model.AppVersion{
		Version:     input.Version,
		Title:       input.Title,
		Message:     input.Message,
		Type:        input.Type,
		BuildNumber: input.BuildNumber,
	}
}
