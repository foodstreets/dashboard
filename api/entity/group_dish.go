package entity

import (
	"time"

	workerClient "gitlab.com/foodstreets/worker/client"

	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/model"
)

type entityGroupDish struct{}

var EntityGroupDish entityGroupDish

type IEntityGroupDish interface {
	Create(input form.GroupDish, slug string) (*model.GroupDish, error)
	GetByID(id int) (*model.GroupDish, error)
}

func (g entityGroupDish) Create(input form.GroupDish, slug string) (groupDish *model.GroupDish, err error) {
	eatery, err := orm.Eatery.GetBySlug(slug)
	if err != nil {
		return
	}
	if eatery == nil {
		return nil, nil
	}

	now := time.Now()
	groupDish = &model.GroupDish{
		Name:      input.Name,
		EateryId:  eatery.Id,
		UpdatedAt: now,
		CreatedBy: 1,
		UpdatedBy: 1,
	}
	err = orm.GroupDish.Create(groupDish)
	if err != nil {
		return
	}
	return
}

func (g entityGroupDish) GetByID(id int) (groupDish *model.GroupDish, err error) {
	defer workerClient.Client.CheckDiscovery(id)

	groupDish, err = orm.GroupDish.GetByID(id)
	if err != nil {
		return
	}
	if groupDish == nil {
		return nil, nil
	}
	return
}
