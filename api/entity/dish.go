package entity

import (
	"time"

	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/model"
)

type entityDish struct{}

var EntityDish entityDish

type IEntityDish interface {
	Create(input form.Dish, groupDishID int) (*model.Dish, error)
	GetByID(id int) (*model.Dish, error)
}

func (g entityDish) Create(input form.Dish, groupDishID int) (dish *model.Dish, err error) {
	now := time.Now()
	dish = &model.Dish{
		Name:        input.Name,
		Slug:        input.Slug,
		GroupDishId: groupDishID,
		Image:       input.Image,
		Price:       input.Price,
		Active:      input.Active,
		UpdatedAt:   now,
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	err = orm.Dish.Create(dish)
	if err != nil {
		return
	}
	return
}

func (g entityDish) GetByID(id int) (dish *model.Dish, err error) {
	dish, err = orm.Dish.GetByID(id)
	if err != nil {
		return
	}

	return
}
