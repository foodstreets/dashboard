package entity

import (
	"fmt"

	newsfeedClient "gitlab.com/foodstreets/newsfeed/init/client"
	newsfeedRequest "gitlab.com/foodstreets/newsfeed/protobuf/request"
	newsfeedResponse "gitlab.com/foodstreets/newsfeed/protobuf/response"

	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/repo"

	"gitlab.com/foodstreets/master/model"

	"time"

	"github.com/gosimple/slug"

	"github.com/gin-gonic/gin"
)

type entityEatery struct{}

var EntityEatery IEntityEatery

type IEntityEatery interface {
	Create(input form.Eatery) (*model.Eatery, error)
	GetByID(id int) (*model.Eatery, error)
	GetElasticByID(id int) (*newsfeedResponse.Eatery, error)
	DeleteByID(id int) error
	Update(id int, input form.Eatery) (*newsfeedResponse.Eatery, error)
	SearchEatery(c *gin.Context) (int, []model.Eatery, error)
}

func init() {
	EntityEatery = &entityEatery{}
}

func (e entityEatery) Create(input form.Eatery) (*model.Eatery, error) {
	now := time.Now()
	userID := 1
	eatery := input.ToModel(userID)
	eatery.CreatedAt = now
	eatery.CreatedBy = 1

	//Move image
	newName := fmt.Sprintf("eatery-food-%s-%d", slug.Make(eatery.Name), time.Now().Unix())
	err := repo.Upload.MoveImageFood("http://localhost:8181/v1", eatery.Image, newName)
	if err != nil {
		return nil, err
	}

	eatery.Image = newName
	err = orm.Eatery.Create(&eatery)
	if err != nil {
		return nil, err
	}
	//	go afterCreate(eatery)
	return &eatery, nil
}

func afterCreate(eatery *model.Eatery) {
	//	rbt := model.RabbitMQ{
	//		Action:     "a",
	//		Task:       "b",
	//		Msg:        "Pls",
	//		ExecutedAt: time.Now(),
	//	}
	//	fmt.Println("rbt", rbt)
	//
	//	err := workerClient.Send(&rbt)
	//	fmt.Println("err", err)
	if eatery == nil {
		fmt.Println("eatery is nil")
		return
	}

	req := newsfeedRequest.Eatery{
		Id: int64(eatery.Id),
	}
	go newsfeedClient.Eatery.Create(&req)
}

func (e entityEatery) GetByID(id int) (eatery *model.Eatery, err error) {
	eatery, err = orm.Eatery.GetByID(id)
	if err != nil {
		return
	}
	if eatery == nil {
		return nil, nil
	}
	return
}

func (e entityEatery) GetElasticByID(id int) (*newsfeedResponse.Eatery, error) {
	req := newsfeedRequest.Eatery{
		Id: int64(id),
	}
	eatery, err := newsfeedClient.Eatery.GetByID(&req)
	return eatery, err
}

func (e entityEatery) DeleteByID(id int) error {
	req := newsfeedRequest.Eatery{
		Id: int64(id),
	}
	_, err := newsfeedClient.Eatery.DeleteByID(&req)
	return err
}

func (e entityEatery) Update(id int, input form.Eatery) (*newsfeedResponse.Eatery, error) {
	req := newsfeedRequest.Eatery{
		Id:   int64(id),
		Name: input.Name,
		//		Slug:    input.Slug,
		//		Active:  input.Active,
		Address: input.Address,
		Image:   input.Image,
	}
	eatery, err := newsfeedClient.Eatery.Update(&req)
	return eatery, err
}

func (entityEatery) SearchEatery(c *gin.Context) (total int, eateries []model.Eatery, err error) {
	name := c.Query("q")
	req := newsfeedRequest.SearchEatery{
		Name: name,
	}
	resp, err := newsfeedClient.Eatery.SearchEatery(&req)
	if err != nil {
		return 0, []model.Eatery{}, err
	}

	eateryIds := make([]int, len(resp.EateryIds))
	for i, eateryId := range resp.EateryIds {
		eateryIds[i] = int(eateryId)
	}

	total, eateries, err = orm.Eatery.GetByIDs(eateryIds)
	return
}
