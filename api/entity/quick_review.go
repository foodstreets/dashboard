package entity

import (
	"encoding/json"
	"fmt"

	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/repo"

	"gitlab.com/foodstreets/master/model"
)

type quickReview struct{}

var QuickReview IQuickReview

type IQuickReview interface {
	Create(input form.QuickReview) (*model.QuickReview, error)
}

func init() {
	QuickReview = &quickReview{}
}

func (a quickReview) Create(input form.QuickReview) (*model.QuickReview, error) {
	quickReview := input.ToModel()
	err := orm.QuickReview.Create(&quickReview)
	if err != nil {
		return nil, err
	}

	go afterCreateQuickReview(&quickReview)
	return &quickReview, nil
}

func afterCreateQuickReview(quickReview *model.QuickReview) {
	if quickReview == nil {
		return
	}

	key := model.KeyRedisQuickReview

	var blob []byte
	blob, err := repo.Redis.GetBytes(key)
	if err != nil {
		fmt.Println(err)
		return
	}
	if blob == nil {
		err = repo.Redis.Set(key, []model.QuickReview{*quickReview}, model.ExpiredTimeKeyQuickReview)
		if err != nil {
			fmt.Println(err)
		}
		return
	}

	src := []model.QuickReview{}
	err = json.Unmarshal(blob, &src)
	if err != nil {
		fmt.Println(err)
		return
	}

	src = append(src, *quickReview)

	//Create key new
	err = repo.Redis.Set(key, src, model.ExpiredTimeKeyQuickReview)
	if err != nil {
		fmt.Println(err)
	}
	return
}
