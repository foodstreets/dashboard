package entity

import (
	"gitlab.com/foodstreets/dashboard/api/form"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/repo"

	"gitlab.com/foodstreets/master/model"
)

type appVersion struct{}

var AppVersion IAppVersion

type IAppVersion interface {
	Create(input form.AppVersion) (*model.AppVersion, error)
}

func init() {
	AppVersion = &appVersion{}
}

func (a appVersion) Create(input form.AppVersion) (*model.AppVersion, error) {
	appVersion := input.ToModel()
	err := orm.AppVersion.Create(&appVersion)
	if err != nil {
		return nil, err
	}

	afterwardCreate(&appVersion)
	return &appVersion, nil
}

func afterwardCreate(appVersion *model.AppVersion) {
	//Cache
	src := model.AppVersion{}
	key := model.KeyRedisAppVersion
	err := repo.Redis.Get(key, &src)
	if err != nil {
		panic(err)
		return
	}

	//Create key new
	err = repo.Redis.Set(key, appVersion, model.ExpiredTimeKey)
	if err != nil {
		panic(err)
	}
	return
}
